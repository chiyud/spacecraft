
#Using BFS solve spacecraft puzzles with different representation. (Two executables)

## Directory structure
. (root) --> saves the runable codes, spacecraft_p1.py for one representation, spacecraft_p2.py for the other representation. Domains are hard-coded into these codes.

Puzzles/ --> contains files for inital states of some puzzles. Puzzle_* for the first representation, p2_puzzle_* for the second representation.

solutions/ --> contains solution files that describe plans by the first representation.

p2_solutions/ --> contains solution files that describe plans by the second representation. 

## Representation_1:
We encoded the domain representation into code. 
### States
States are tuple of spacecrafts with their location.
### Actions
Actions:
	precondition: moving spacecrafts S (at X_s,Y_s) in one of directions is valid
		and   get the valid target location X, Y
 	effect:
		add At[S,X,Y]
		remove At[S,X_s,Y_s]

###Goal
Goal is [R,2,2] which means that Red spacecrafts locates at [2,2] cell.
 
###Execute

```bash
$ python spacecraft_p1.py Puzzle_xy
```
You don't need to specify folder names, name of Puzzle_xy is enough.

###Example
where Puzzle_xy is puzzle configuration file with the following format:

Red_x,	Red_y

Orange_x, Orange_y

Green_x, Green_y

Purple_x, Purple_y

Yellow_x, Yellow_y

Blue_x, Blue_y


Note the ORDER of colors makes a big difference, do not reorder them.

##2 Representation_2:
We also encoded this domain representation into the code spacecraft_p2.py
### States
States are board with spacecrafts occupy cells. Empty cells labelled as '0'
### Actions
Actions are changing either one cell in the board from empty to S in spacecrafts.
	if the changing is valid, then make the change and set S's previous cell as zero
### Goal
```
*,*,*,*,*
*,*,*,*,*
*,*,R,*,*
*,*,*,*,*
*,*,*,*,*
```
where * can be any feasible element in the cell, either 0 or spacecrafts.
###Execute

```bash
$ python spacecraft_p2.py p2_Puzzle_xy
```
You don't need to specify folder names, name of p2_Puzzle_xy is enough.

###Example
Example of states: (puzzle 9)
```
0,0,R,0,O
0,0,0,0,0
0,0,G,0,0
0,P,0,0,0
0,0,0,Y,0
```

