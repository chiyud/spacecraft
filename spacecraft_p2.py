#!/usr/bin/python
'''
Solving the spacecraft puzzels using BFS
Copyright (C) 2016  Chiyu Dong

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
from __future__ import print_function
import sys

#  define node field

class state:
    def __init__(self,cur_crafts, parent = None):
        self.crafts = cur_crafts
        self.par = parent
        self.code = ''
        # encode a key for visited state
        for i in range(len(self.crafts)):
            coor = self.crafts[i]
            y = coor[0]
            x = coor[1]
            self.code += chr((y*5+x)+ord('a'))
            
        self.getMove()
        
    def getMove(self, index = -1, From = [], To = [], Direction=""):
        self.moveNode = index
        self.moveFrom = From
        self.moveTo = To
        self.Dir = Direction


#  read puzzle from files
def getPuzzle(filename):
    rst = []
    with open(filename,'r') as f:
        for line in f:
            l = list(line[0:-1])
            cur = [int(l[0]),int(l[-1])]
            rst.append(cur)
            
    return rst

def processPuzzle(filename):
    cur_rst = []
    with open(filename,'r') as f:
        r = 0
        for line in f:
            c = 0
            l = list(line[0:-1])
            for item in l:
                if item!='0' and item!='\n' and item!=',' :
                    cur = [item,r,int(c/2)]
                    cur_rst.append(cur)
                    
                c +=1
            r+=1
    ## rearrange R,O,G,P,Y,B
    rst = cur_rst[:]
    for item in cur_rst:
        if item[0] == 'R':
            rst[0] = item[1:3]
        elif item[0] == 'O':
            rst[1] = item[1:3]
        elif item[0] == 'G':
            rst[2] = item[1:3]
        elif item[0] == 'P':
            rst[3] = item[1:3]
        elif item[0] == 'Y':
            rst[4] = item[1:3]
        elif item[0] == 'B':
            rst[5] = item[1:3]
    
    return rst
    
# check valid move in 4 directions
# return the valid move or [-1,-1]
def checkValid(direction,cur_pos, cur_crafts):
    cur_y = cur_pos[0]
    cur_x = cur_pos[1]
    
    if direction=='up' :
        while(cur_y>0):
            if [cur_y-1,cur_x] in cur_crafts:
                return [cur_y,cur_x]
            else:
                cur_y -= 1
                
    if direction =='down':
        while(cur_y<4):
            if [cur_y+1,cur_x] in cur_crafts:
                return [cur_y,cur_x]
            else:
                cur_y += 1
    
    if direction =='right':
        while(cur_x<4):
            if [cur_y,cur_x+1] in cur_crafts:
                return [cur_y,cur_x]
            else:
                cur_x += 1
        
    if direction =='left':
        while(cur_x>0):
            if [cur_y,cur_x-1] in cur_crafts:
                return [cur_y,cur_x]
            else:
                 cur_x -=1
    
    return [-1,-1]


##############################
##                          ##
## main function goes below ##
##                          ##
##############################

if len(sys.argv)<2:
    print("\n[Error input]\nPlease input correct initial state files:\n For Example:")
    print(" $ python spacecraft_p2.py p2_Puzzle_*\n")
    exit()
print('Start')

# read in spacecraft's location from files
filename = sys.argv[1]
if filename[0:2]!='p2':
    print("wrong reprensentation file\n")
    exit()
    

#crafts = getPuzzle(filename) # just for testing

crafts = processPuzzle('Puzzles/'+filename)

# define possible moving directions
directions = ['up','down','right','left']

# initialize BFS root
root = state(crafts)

# BFS Queue
Q = []	
Q.append(root)

# set visited hashtable. 
# If the current node has not been visited, then add its code to this map.
# Else, skip the current node 
visited = {}
visited[root.code] = True

# Start the BFS
while(Q):
    # pop out and get the head of the queue
    cur_States= Q.pop(0)
    
    # iterate every spacecraft
    for s in range(len(cur_States.crafts)):
        S = cur_States.crafts[s]
        # iterate every direction for the current space craft
        for Dir in directions:
            # get the possible target position of the spacecraft given direction.
            # if the result "new_state" is [-1,-1], this direction is illegal
            # else, it will result possible target position
            new_state = checkValid(Dir,S,cur_States.crafts)
                
            if new_state[0]>-1:
                # get new state and its node
                new_crafts = cur_States.crafts[:]
                From = new_crafts[s]
                new_crafts[s] = new_state
                # establish new node, and point to its parent
                new_node = state(new_crafts,parent=cur_States)
                new_node.getMove(s,From,new_state,Dir)
                
                # if the new established state has not been visited,
                #   add it to the queue and the visited hashtable
                if new_node.code not in visited:
                    Q.append(new_node)
                    visited[new_node.code] = True
                else:
                    continue
                
            if s == 0 and new_state == [2,2]:
                print('succeed!')
                break   

        else: 
            continue
        
        break
    else:
        continue
    
    break

# backtrace the result
SS = new_node
rst =  []
crafts_name = {0:'Red',1:'Orange',2:'Green',3:'Purple',4:'Yellow',5:'Blue'}


## just for visualization and write result to files
while(SS.par!=None):
    string = 'Set '+ str(SS.moveTo)+' to ' + str(crafts_name[SS.moveNode]) +',\t Clear '+str(SS.moveFrom) 
    print(string)
    rst.insert(0,string)
    SS = SS.par

solution_file = 'p2_solutions/Solution_'+filename

with open(solution_file,'w') as f:
    f.write('Board transitions\n')
    for line in rst:
        f.write(line)
        f.write('\n')



